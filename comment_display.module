<?php

/**
 * @file
 * Prevents rendering of comments inside of nodes and provides them as separate
 * page elements instead.
 */

/**
 * Implementation of hook_menu_alter().
 *
 * Alters node.module's node view page callbacks to use ours instead.
 */
function comment_display_menu_alter(&$callbacks) {
  // Skip this if Page Manager exists; comment_display_preprocess_page() will
  // still work.
  if (!module_exists('page_manager')) {
    $callbacks['node/%node']['page callback'] = 'comment_display_node_page_view';
    $callbacks['node/%node/revisions/%/view']['page callback'] = 'comment_display_node_show';
  }
}

/**
 * Generate a page displaying a single node, along with its comments.
 *
 * This is identically to node's implementation, but just does not append
 * comments of the node.
 */
function comment_display_node_show($node, $cid, $message = FALSE) {
  if ($message) {
    drupal_set_title(t('Revision of %title from %date', array('%title' => $node->title, '%date' => format_date($node->revision_timestamp))));
  }
  $output = node_view($node, FALSE, TRUE);

  // Note: Output of comments moved into comment_display().

  // Update the history table, stating that this user viewed this node.
  node_tag_new($node->nid);

  return $output;
}

/**
 * Menu callback; view a single node.
 *
 * Use our callback instead of node_show().
 */
function comment_display_node_page_view($node, $cid = NULL) {
  drupal_set_title(check_plain($node->title));
  return comment_display_node_show($node, $cid);
}

/**
 * Implementation of hook_preprocess_page().
 *
 * Provide node comments, if available, as $comments variable to theme.
 */
function comment_display_preprocess_page(&$vars) {
  // Prevent overriding a 'comments' region.
  if (isset($vars['comments'])) {
    return;
  }
  $vars['comments'] = '';
  if (!empty($vars['node'])) {
    $vars['comments'] .= comment_display($vars['node']);

    // Reconstruct CSS and JS variables.
    $vars['css'] = drupal_add_css();
    $vars['styles'] = drupal_get_css();
    $vars['scripts'] = drupal_get_js();
  }
}

/**
 * Implementation of hook_block().
 */
function comment_display_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks['comments'] = array(
      'info' => t('Comments'),
      'region' => 'content',
      'weight' => 10,
      'status' => 1,
    );
    $blocks['comment_form'] = array(
      'info' => t('Comment form'),
      'region' => 'content',
      'weight' => 11,
      'status' => 1,
    );
    return $blocks;
  }
  else if ($op == 'view') {
    $node = menu_get_object();
    if (!empty($node)) {
      if ($delta == 'comments') {
        $block['subject'] = t('Comments');
        $block['content'] = comment_display($node);
      }
      elseif ($node->comment == COMMENT_NODE_READ_WRITE) {
        $block['subject'] = t('Post new comment');
        $block['content'] = drupal_get_form('comment_form', array('nid' => $node->nid));
      }
    }
    return $block;
  }
}

/**
 * Helper function to render and return comments.
 *
 * @param $node
 *   A fully loaded node object.
 */
function comment_display($node) {
  static $output;
  
  if (isset($output)) {
    return $output;
  }
  $output = '';
  if (function_exists('comment_render') && $node->comment) {
    $arg2 = arg(2);
    $comments = comment_render($node, ($arg2 && is_numeric($arg2) ? $arg2 : NULL));
    // Testing the length is awkward, but defining an alternate template just to
    // prevent the block from displaying when empty would be even more awkward.
    if (strlen($comments) > 50) {
      $output .= $comments;
    }
  }
  return $output;
}

